#include <sys/types.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/stat.h>
#include <unistd.h>
#include <ctype.h>
#include <time.h> 


int main (int argc, char *argv[])
{
	srand (1);
	int fd = open("/dev/pmem0", O_RDWR, S_IRUSR | S_IWUSR);
	struct stat sb;

	if(fstat(fd, &sb)==-1)
	{
		perror("couldn't get file size.\n");
		exit(0);
	}
	sb.st_size = atoi(argv[1]);
	printf("Size of block:%ld\n",sb.st_size);
	char *file_in_memory = mmap(NULL, sb.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

#ifdef CLEAR_DF
	printf("Clean\n");
	for (int i=0; i<sb.st_size;i++)
	{
		file_in_memory[i] = 0x00;	
	}
	munmap(file_in_memory, sb.st_size);
	close(fd);
#else
	printf("Write\n");
	for (int i=0; i<sb.st_size;i++)
	{
		file_in_memory[i]= rand()%25+97; //letras de la 'a' a la 'z' en minusculas (código ascii)
		//printf("%c\t",file_in_memory[i]);//visualización
	}
	munmap(file_in_memory, sb.st_size);
	close(fd);
#endif
}
