#include <sys/types.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/stat.h>
#include <unistd.h>
#include <ctype.h>

int main (int argc, char** argv)
{
	int fd = open("/dev/pmem0", O_RDWR, S_IRUSR | S_IWUSR);
	struct stat sb;

	if(fstat(fd, &sb)==-1)
	{
		perror("couldn't get file size.\n");
		exit(0);
	}
	sb.st_size=1<<27; //100MB
	int *file_in_memory =mmap(NULL, sb.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	//printf("pid: %d\n",getpid());
	//printf("Dir virtual: %lx\n", (long)file_in_memory);
#ifdef CLEAR_DF
	for (int i=0; i<sb.st_size;i++)
	{
		if((i%2)==0){
			file_in_memory[i]= NULL;
		}
	}
	munmap(file_in_memory, sb.st_size);
	close(fd);
	exit(0);
#else
	for (int i=0; i<sb.st_size;i++)
	{
		if((i%2)==0){
			file_in_memory[i]= rand()%100;
		}
	}
	munmap(file_in_memory, sb.st_size);
	close(fd);
	exit(0);
#endif
	
}
